Shader "Custom/FullScreenQuad" {
    Properties{
            _MainTex ("Texture", 2D) = "gray" {}
    }

    SubShader{
        /* We want this shader to be rendered last otherwhise the scene we want to draw will be 
         * drawn on top of it
         */
        Tags{ 
            "Queue" = "Transparent"
            "RenderType" = "Opaque"
        }

        Pass{
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag
            

            sampler2D _MainTex;

            struct vertInput {
                float4 pos : POSITION;
                float4 uv : TEXCOORD0;
                uint id : SV_VertexID;  
            };
             
            struct vertOutput {
                float4 pos : SV_POSITION;
                float4 uv : TEXCOORD0;
            };
             
            // Vertex shader
            vertOutput vert(vertInput input) {
                vertOutput output;

                /* The coordinates of a quad wrt to it's parent are: 
                 *  [ (-.5, -.5), (.5, .5), (.5, -.5), (-.5, -5) ]
                 * Thus, to make a fullscreen quad we only need to multiply the coordinates by 2
                 */
                output.pos = float4(input.pos.xy * 2, 1, 1);
                output.uv = input.uv;
            
                return output;
            }

            // Fragment shader
            float4 frag(vertOutput input) : COLOR {
                return tex2D(_MainTex, input.uv);
            }
            ENDCG
        }
    }
}
