﻿
using System;
using System.Collections;
using System.Runtime.InteropServices;

using UnityEngine;

// This enumeration should be kept in sync with the one in 
//  ros_interface_plugin/include/ros_interface_plugin/davinci_console.hpp
//
// Any differences will cause the wrong image to be sourced
public enum DaVinciCameraID {
    None = 0,
    EndoscopeLeft = 1,
    EndoscopeRight = 2
}

public class DaVinciConsole : MonoBehaviour {   

    #region PInvokes

    [DllImport("ros_interface_plugin", EntryPoint = "ros_check")]
    static private extern bool ROSCheck();

    [DllImport("ros_interface_plugin", EntryPoint = "davinci_console_create")]
    static private extern IntPtr CreateDaVinciConsoleInstance();

    [DllImport("ros_interface_plugin", EntryPoint = "davinci_console_dispose")]
    static private extern void DisposeDaVinciConsoleInstance(IntPtr console_ptr); 

    [DllImport("ros_interface_plugin", EntryPoint = "davinci_console_setup_cameras")]
    static private extern void SetupCameras(IntPtr console_ptr); 

    [DllImport("ros_interface_plugin", EntryPoint = "davinci_console_endoscope_has_images")]
    static private extern bool EndoscopeHasImagesImpl(IntPtr console_ptr);

    [DllImport("ros_interface_plugin", EntryPoint = "davinci_console_get_endoscope_image_count")]
    static private extern int EndoscopeGetImageCountImpl(IntPtr console_ptr);

    [DllImport("ros_interface_plugin", EntryPoint = "davinci_console_update_texture")]
    static private extern void UpdateCameraTextureImpl(
        IntPtr console_ptr, IntPtr texture_handle, int texture_width, int texture_height, int camera);

    #endregion

    #region Private Members

    private IntPtr _native_instance = IntPtr.Zero;

    private float _creation_timestamp = 0f;

    private bool _endoscope_has_images = false;

    #endregion

    #region Callbacks

    void Awake () {
        if (!ROSCheck()) {
            Debug.LogError("Unable to contact ROS master. Any ROS dependent functions will be broken");
            return;
        }

        this._native_instance = CreateDaVinciConsoleInstance();
        if (this._native_instance == IntPtr.Zero) {
            throw new Exception("Unable to create new DaVinciConsole instance. Creation failed.");
        }

        this._creation_timestamp = Time.realtimeSinceStartup;        
        Debug.LogFormat("Created new DaVinciConsole instance @ 0x{0:X8}", this._native_instance.ToInt64());
        
        // Start the cameras -  We need to monitor them to make sure that we are actually receiving
        // images and the everything is running smoothly 
        SetupCameras(this._native_instance);
        StartCoroutine(CheckCameraImages());
    }

    void OnDestroy() {
        if (this._native_instance != IntPtr.Zero) {
            DisposeDaVinciConsoleInstance(this._native_instance);
            this._native_instance = IntPtr.Zero;
        }
    }

    void Update(){        
        // Do Nothing
    }

    #endregion 

    #region Public Methods 

    public bool EndoscopeHasImages() {
        if (_native_instance == IntPtr.Zero)
            return false;

        if (_endoscope_has_images) 
            return true;

        // If we haven't seen any images yet we can check again to see if we have some now
        _endoscope_has_images = EndoscopeHasImagesImpl(this._native_instance);
        return _endoscope_has_images;
    }

    public int EndoscopeImageCount() {
        if (_native_instance == IntPtr.Zero) { 
            throw new InvalidOperationException();
        }

        return EndoscopeGetImageCountImpl(this._native_instance);
    }

    public void UpdateCameraTexture(Texture2D dest, DaVinciCameraID camera_id) {
        if (_native_instance == IntPtr.Zero) { 
            throw new InvalidOperationException();
        }

        IntPtr native_ptr = dest.GetNativeTexturePtr();
        UpdateCameraTextureImpl(
            _native_instance, native_ptr, dest.width, dest.height, (int) camera_id
        );
    }

    #endregion

    #region CoRoutines 

    IEnumerator CheckCameraImages() {
        if (_native_instance == IntPtr.Zero) {
            throw new InvalidOperationException();
        }

        while (!EndoscopeHasImagesImpl(_native_instance)) {
            float age = Time.realtimeSinceStartup - this._creation_timestamp;
            if (age > 1f) {
                Debug.LogWarningFormat("DaVinciConsole has not recieved any images from the endoscope after {0} seconds!", age);
            }

            yield return new WaitForSeconds(10f);
        }
    }

    #endregion

}
