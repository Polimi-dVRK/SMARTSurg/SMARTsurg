

using System;
using System.Collections;
using System.Runtime.InteropServices;

using UnityEngine;

public class DaVinciCameraBackgroundShader : MonoBehaviour {   
    
    /// This class creates a shader where the camera images are rendered. Images are always displayed on top, however
    /// the GUI cannot be displayed on top of the shader. If you want to overlay a GUI use
    /// DaVinciCameraBackgroundFrustum instead.

    #region Public Members

    /// The DaVinciConsole instance to interact with
    public GameObject DaVinciConsole = null;

    /// The image that should be displayed on the background quad
    public DaVinciCameraID SourceImageID = 0;

    /// The quad on which to draw the image
    public GameObject DestinationQuad = null;

    #endregion

    #region Private Members

    private DaVinciConsole _davinci_console = null; 
    
    /// The texture we will actually draw. We need to own it since we're rewriting its contents at
    /// each frame. 
    private Texture2D _my_texture = null;  

    private Material _destination_quad_material = null;
    private Shader _full_screen_quad_shader = null;

    #endregion


    void Awake () {
        if (!DaVinciConsole) {
            throw new Exception("DaVinciCameraTexture::DaVinciConsole must not be null");
        }

        if (!DestinationQuad) {
            throw new Exception("DaVinciCameraTexture::ImageQuad member must not be null");
        }

        _davinci_console = DaVinciConsole.GetComponent<DaVinciConsole>();
        if (!_davinci_console) {
            throw new Exception("Specified `DaVinciConsole` does not contain a `DaVinciConsole` script component");
        }

        // Add the fullscreen quad shader to the destination quad so that it always appears to be
        // fullscreen
        _destination_quad_material =  DestinationQuad.GetComponent<Renderer>().material;

        _full_screen_quad_shader = Shader.Find("Custom/FullScreenQuad");
        if (!_full_screen_quad_shader) {
            throw new Exception("Unable to locate shader Custom/FullScreenQuad");
        }
        _destination_quad_material.shader = _full_screen_quad_shader;
    }

    void Update() {     
        if (!_davinci_console) return; 

        // Only update the texture if we know that we have something to show
        if (SourceImageID != DaVinciCameraID.None && _davinci_console.EndoscopeHasImages()) {
            if (!_my_texture) {
                _my_texture = new Texture2D(720, 576, TextureFormat.RGB24, false);
                _my_texture.filterMode = FilterMode.Point;
                _my_texture.Apply();

                _destination_quad_material =  DestinationQuad.GetComponent<Renderer>().material;
                _destination_quad_material.mainTexture = _my_texture;

                if (!_full_screen_quad_shader) {
                    _destination_quad_material.shader = _full_screen_quad_shader;
                }
            }

            _davinci_console.UpdateCameraTexture(_my_texture, SourceImageID);
        }
    }
}
