﻿using System;
using UnityEngine;

public class DaVinciCameraBackgroundFrustum: MonoBehaviour {   
    
    /// This class applies the camera texture to a quad with a proper size. The plane is authomatically centered and 
    /// scaled wrt the camera and is placed at a given ditsnce from it.  

    #region Public Members

        /// The DaVinciConsole instance to interact with
        public GameObject DaVinciConsole;
    
        /// The image that should be displayed on the plane
        public DaVinciCameraID SourceImageID = 0;

        /// The camera that should look at the plane
        public Camera CurrentCamera;

        /// Texture to display until the image form the camera is arrived
        public Texture WaitForImageTexture;

        public float DistanceFromCamera;
        public int TextureWidth; // Davinci Endoscope is 720
        public int TextureHeight; // Davinci Endoscope is 576

    #endregion

    #region Private Members

        private DaVinciConsole _davinciConsole; 
        
        /// The texture we will actually draw. We need to own it since we're rewriting its contents at
        /// each frame. 
        private Texture2D _myTexture;  
    
        private Material _destinationPlaneMaterial;
    
        /// The plane on which to draw the image
        private GameObject _destinationQuad;

    #endregion


    private void Awake () {
        if (!DaVinciConsole) {
            throw new Exception("DaVinciCameraTexture::DaVinciConsole must not be null");
        }

        _davinciConsole = DaVinciConsole.GetComponent<DaVinciConsole>();
        if (!_davinciConsole) {
            throw new Exception("Specified `DaVinciConsole` does not contain a `DaVinciConsole` script component");
        }
        
        _destinationQuad = GameObject.CreatePrimitive( PrimitiveType.Quad );
        _destinationQuad.transform.parent = CurrentCamera.transform;
        switch (SourceImageID) {
            case DaVinciCameraID.EndoscopeLeft:
                _destinationQuad.layer = LayerMask.NameToLayer("Endoscope Left Eye");
                break;
            case DaVinciCameraID.EndoscopeRight:
                _destinationQuad.layer = LayerMask.NameToLayer("Endoscope Right Eye");
                break;
            case DaVinciCameraID.None:
                _destinationQuad.layer = LayerMask.NameToLayer("Default");
                break;
        }
        
        // Add the fullscreen quad shader to the destination quad so that it always appears to be
        // fullscreen
        CreateQuad(DistanceFromCamera);
        _destinationPlaneMaterial =  _destinationQuad.GetComponent<Renderer>().material;
        _destinationPlaneMaterial.shader = Shader.Find("Unlit/Texture");
        _destinationPlaneMaterial.mainTexture = WaitForImageTexture;
    }

    private void Update() {     
        if (!_davinciConsole) return; 

        // Only update the texture if we know that we have something to show
        if (SourceImageID != DaVinciCameraID.None && _davinciConsole.EndoscopeHasImages()) {
            if (!_myTexture) {
                _myTexture = new Texture2D(TextureWidth, TextureHeight, TextureFormat.RGB24, false);
                _myTexture.filterMode = FilterMode.Point;
                _myTexture.Apply();

                _destinationPlaneMaterial =  _destinationQuad.GetComponent<Renderer>().material;
                _destinationPlaneMaterial.mainTexture = _myTexture;
                
                // Opencv images have the oringin in the top left corner, but opengl in the bottom right corner.
                // The Opencv images from cameras are horizzontally flipped when copied into opengl.
                _destinationPlaneMaterial.SetTextureScale("_MainTex" ,new Vector2(1, -1));
            }

            _davinciConsole.UpdateCameraTexture(_myTexture, SourceImageID);
        }
    }

    private void CreateQuad(float distanceFromCamera) {
        var frustumCorners = new Vector3[4];
        var worldSpaceCorners = new Vector3[4];
        var normals = new Vector3[4];
        CurrentCamera.CalculateFrustumCorners(new Rect(0, 0, 1, 1), distanceFromCamera, Camera.MonoOrStereoscopicEye.Mono, frustumCorners);
        for (var i = 0; i < 4; i++) {
            worldSpaceCorners[i] = CurrentCamera.transform.TransformVector(frustumCorners[i]);
            // Debug.Log("Corner: " + worldSpaceCorners[i]);
            normals[i] = Vector3.forward;
        }
        var mesh = new Mesh();
        _destinationQuad.GetComponent<MeshFilter>().mesh = mesh;
        
        var uv = new Vector2[4];
        uv[0] = new Vector2(0, 0);
        uv[1] = new Vector2(0, 1);
        uv[2] = new Vector2(1, 1);
        uv[3] = new Vector2(1, 0);
        
        var tri = new[] {0, 1, 3, 3, 1, 2};
        
        mesh.vertices = worldSpaceCorners;
        mesh.uv = uv;
        mesh.triangles = tri;
        mesh.normals = normals;
    }
}
