﻿using System;
using System.Collections.Generic;
using System.Xml;
using AsImpL;
using UnityEngine;
using UnityEngine.UI;

public class ImportPreoperative : MonoBehaviour {
		
	public Slider TransparencySlider;
	public Toggle MeshToggle;
	public Toggle WireframeToggle;
	public Toggle PointsToggle;
	public 	GameObject WorkspaceCube;
	public GameObject ModelWindow;


	private class ModelInfo {
		public string Name;
		public Color Color;
	}
		
	private enum BlendMode{
		Opaque,
		Cutout,
		Fade,
		Transparent
	}
		
	private enum MeshMode{
		Triangles,
		Lines,
		Points,
	}
		
	private string _folderPath;
	public ImportOptions ImportOptions = new ImportOptions();
	private ObjectImporter _objImporter;
	private bool _justFinished = true;
	private ModelInfo[] _modelsToImport;
	private readonly List<GameObject> _importedObjsList = new List<GameObject>();
	
	/**
		 * Add listeners to the slider and the toggles used to set the model rendering
		 * The object importer is created to upload the preoperative models. It works only for ".obj" files.
		 * Models to import are specified in a ".xml" file. It is parsed and then the models are imported asynchronous
		 * in a coroutine.
		 */
	private void Awake(){
		if (Application.platform == RuntimePlatform.LinuxEditor){
			_folderPath = Application.dataPath + "/../../preoperative_models/";
		}
		else if (Application.platform == RuntimePlatform.LinuxPlayer){
			_folderPath = Application.dataPath + "/preoperative_models/";
		}
			
		// make the workspace cube transparent
		Renderer workspaceCubeRenderer = WorkspaceCube.GetComponent<Renderer>();
		workspaceCubeRenderer.enabled = false;
			
		// add listeners to slider and toggles to set the preoperative-models parameters
		TransparencySlider.onValueChanged.AddListener(delegate {TransparencySliderValueChangeCheck(); });
		MeshToggle.onValueChanged.AddListener(delegate{
			MeshToggleValueChanged(ref MeshToggle, MeshMode.Triangles);
		});
		WireframeToggle.onValueChanged.AddListener(delegate{
			MeshToggleValueChanged(ref WireframeToggle, MeshMode.Lines);
		});
		PointsToggle.onValueChanged.AddListener(delegate{
			MeshToggleValueChanged(ref PointsToggle, MeshMode.Points);
		});
			
		// import preoperative models
		_objImporter = gameObject.AddComponent<ObjectImporter>();
		_modelsToImport = ParseLoaderXml().ToArray();
		ImportModelListAsync(ref _modelsToImport);
	}
		
	private void Start(){
		// Because of the coroutine, everything is written after the importing command is not executed.
	}
		
	/**
		 * Once the loading of all the models is finished, they are grouped, scaled and translated in order to appear
		 * in the correct position within the GUI
		*/
	private void Update(){
		var loading = Loader.totalProgress.fileProgress.Count > 0;
		if (!loading && _justFinished){
			_justFinished = false;
			GameObject[] importedObjects = GetImportedObjects(ref _modelsToImport);
			GroupScaleAndTranslateModels(ref importedObjects);
		}
	}
		
	/**
		 * Import all the models 
		*/
	private void ImportModelListAsync(ref ModelInfo[] modelsInfo){
		if (modelsInfo == null){
			return;
		}
		for (var i = 0; i < modelsInfo.Length; i++){
			// importOptions.reuseLoaded = reuseLoaded;
			string modelName = modelsInfo[i].Name;
			string modelPath = _folderPath + modelName + ".obj";
			_objImporter.ImportModelAsync(modelName, modelPath, null, ImportOptions);
		}
	}
		
	/**
		 * Parse the xml files containing the models to upload. Each <model> node contains children nodes that
		 * characterize the model.
		 * The name of the file is contained in the <name>. The file name becomes the object name into unity.
		*/
	private List<ModelInfo> ParseLoaderXml(){
		var modelsList = new List<ModelInfo>();
		var doc = new XmlDocument();
		doc.Load(_folderPath + "models_list.xml");
		var nodes = doc.DocumentElement.SelectNodes("/document/model");
		
		foreach(XmlNode node in nodes){
			ModelInfo currentModelInfo = new ModelInfo();
				
			// obj name
			string currentName = node.SelectSingleNode("name").InnerText;
			currentModelInfo.Name = currentName;
				
			// obj color
			if (node.SelectSingleNode("color") != null){
				string currentColorName = node.SelectSingleNode("color").InnerText;
				currentModelInfo.Color = StringToColor(ref currentColorName);
			}
			modelsList.Add(currentModelInfo);
		}
		return modelsList;
	}
		
	/**
		 * Get all the loaded models
		*/
	private GameObject[] GetImportedObjects(ref ModelInfo[] modelsInfo){
		GameObject[] objs = new GameObject[modelsInfo.Length];
		for (var i = 0; i < modelsInfo.Length; i++){
			objs[i] = GameObject.Find(modelsInfo[i].Name);
			objs[i].transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().material.color = 
				modelsInfo[i].Color;
		}
		return objs;
	}

	/**
		 * Print scale and position of each loaded model
		*/
	private void PrintModelsScaleAndPosition(ref GameObject[] objs){
		for (var i = 0; i < objs.Length; i++){
			GameObject child = objs[i].transform.GetChild(0).gameObject;
			child.AddComponent<BoxCollider>();
			MeshCollider currentObjCollider = child.GetComponent<MeshCollider>();
			Debug.Log(objs[i].name + "-> extents: " + currentObjCollider.bounds.extents + " " +
			          "; center: " + currentObjCollider.bounds.center + "; size: " + currentObjCollider.bounds.size);
		}
	}
		
	/**
		 * Groups and scales the loaded models and place them in the correct position 
		*/
	private void GroupScaleAndTranslateModels(ref GameObject[] objs){
		// Creates an object that contains all the models and estimates its size and position
		GameObject modelsContainer = new GameObject("PreoperativeModels");
		modelsContainer.layer = LayerMask.NameToLayer("Preoperative Models");
		modelsContainer.AddComponent<BoxCollider>();
		Bounds ContainerBounds = modelsContainer.GetComponent<BoxCollider>().bounds;
		Bounds ContainerBoundsNew = new Bounds (Vector3.zero, Vector3.zero);
		for (var i = 0; i < objs.Length; i++){
			objs[i].transform.parent = modelsContainer.transform;
			objs[i].layer = LayerMask.NameToLayer("Preoperative Models");
			GameObject child = objs[i].transform.GetChild(0).gameObject;
			child.layer = LayerMask.NameToLayer("Preoperative Models");
			child.GetComponent<MeshRenderer>().material.shader = Shader.Find("Standard");
			ChangeRenderMode(child.GetComponent<MeshRenderer>().material, BlendMode.Transparent);
			// child.GetComponent<MeshRenderer>().material.color = new Color(1, 0.64F, 0, 0.5F);
			// child.GetComponent<MeshRenderer>().material.color =
			//  	ChangeAlpha(child.GetComponent<MeshRenderer>().material.color, 0.5F);
				
			child.AddComponent<BoxCollider>();
				
			ChangeObjectTransparency(ref child, 1);
			ChangeMeshMode(ref child, MeshMode.Triangles);
				
			Bounds currentObjBounds = child.GetComponent<BoxCollider>().bounds;
			ContainerBoundsNew.Encapsulate(currentObjBounds);
			_importedObjsList.Add(child);
		}

		ContainerBounds = ContainerBoundsNew;
			
		// Get the information needed to properly display the objects within the fov of the GUI camera
		Renderer workspaceCubeRenderer = WorkspaceCube.GetComponent<Renderer>();
		float maxSz = Math.Max(Math.Max(ContainerBounds.size.x, ContainerBounds.size.y), ContainerBounds.size.z);
		float scaleFactor = workspaceCubeRenderer.bounds.size.x / maxSz;
			
		// Change scale and position of models
		modelsContainer.transform.position = workspaceCubeRenderer.bounds.center;
		modelsContainer.transform.localScale = new Vector3(scaleFactor, scaleFactor, scaleFactor);
		// workspaceCubeRenderer.enabled = false;
	}
		
	/**
		 * Invoked when the value of the slider changes.
		*/
	private void TransparencySliderValueChangeCheck() {
		// change transparency of window showing preoperative models
		//ModelWindow.GetComponent<RawImage>().CrossFadeAlpha(TransparencySlider.value, 0, false);
			
		// change transparency of real objects => use this for intraoperative models
		foreach (var obj in _importedObjsList){
			var tempobj = obj;
			ChangeObjectTransparency(ref tempobj, TransparencySlider.value);
		}
	}
		
	/**
		 * Invoked when the value of a toggle changes.
		*/
	private void MeshToggleValueChanged(ref Toggle tog, MeshMode mode){
		if (tog.isOn){
			foreach (GameObject obj in _importedObjsList){
				GameObject tempobj = obj;
				ChangeMeshMode(ref tempobj, mode);
			}
		}
	}
		
	/**
		 * Change the object visualization between mesh, wireframe and points
		*/
	private static void ChangeMeshMode(ref GameObject obj, MeshMode mode){
		MeshFilter meshF = obj.GetComponent<MeshFilter>();
		switch (mode){
			case MeshMode.Triangles:
				meshF.mesh.SetIndices(meshF.mesh.GetIndices(0), MeshTopology.Triangles, 0);
				return;
			case MeshMode.Lines:
				meshF.mesh.SetIndices(meshF.mesh.GetIndices(0), MeshTopology.Lines, 0);
				return;
			case MeshMode.Points:
				meshF.mesh.SetIndices(meshF.mesh.GetIndices(0), MeshTopology.Points, 0);
				return;
			default:
				meshF.mesh.SetIndices(meshF.mesh.GetIndices(0), MeshTopology.Triangles, 0);
				return;
		}
	}
		
	private void ChangeObjectTransparency(ref GameObject obj, float alpha){
		obj.GetComponent<MeshRenderer>().material.color =
			ChangeAlpha(obj.GetComponent<MeshRenderer>().material.color, alpha);
	}
		
	private Color ChangeAlpha(Color color, float newAlpha){
		color.a = newAlpha;
		return color;
	}
		
 
	private static void ChangeRenderMode(Material standardShaderMaterial, BlendMode blendMode){
		switch (blendMode){
			case BlendMode.Opaque:
				standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
				standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
				standardShaderMaterial.SetInt("_ZWrite", 1);
				standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
				standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
				standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
				standardShaderMaterial.renderQueue = -1;
				break;
			case BlendMode.Cutout:
				standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
				standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
				standardShaderMaterial.SetInt("_ZWrite", 1);
				standardShaderMaterial.EnableKeyword("_ALPHATEST_ON");
				standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
				standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
				standardShaderMaterial.renderQueue = 2450;
				break;
			case BlendMode.Fade:
				standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
				standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
				standardShaderMaterial.SetInt("_ZWrite", 0);
				standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
				standardShaderMaterial.EnableKeyword("_ALPHABLEND_ON");
				standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
				standardShaderMaterial.renderQueue = 3000;
				break;
			case BlendMode.Transparent:
				standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
				standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
				standardShaderMaterial.SetInt("_ZWrite", 0);
				standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
				standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
				standardShaderMaterial.EnableKeyword("_ALPHAPREMULTIPLY_ON");
				standardShaderMaterial.renderQueue = 3000;
				break;
			default:
				standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
				standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
				standardShaderMaterial.SetInt("_ZWrite", 1);
				standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
				standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
				standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
				standardShaderMaterial.renderQueue = -1;
				break;
		}
	}

	private static Color StringToColor(ref string colorName){
		Color realColor;
		switch (colorName){
			case "blue":
				realColor = Color.blue;
				break;
			case "cyan":
				realColor = Color.cyan;
				break;
			case "green":
				realColor = Color.green;
				break;
			case "magenta":
				realColor = Color.magenta;
				break;
			case "red":
				realColor = Color.red;
				break;
			case "yellow":
				realColor = Color.yellow;
				break;
			default:
				realColor = Color.grey;
				break;
		}

		return realColor;
	}
		
}