﻿using System;
using System.Text;
using System.Collections;
using System.Runtime.InteropServices;

using UnityEngine;

[StructLayout(LayoutKind.Sequential)]
struct CartesianPosition {
    public double pos_x, pos_y, pos_z;
    public double rot_x, rot_y, rot_z;
}

public class DaVinciRobotArm : MonoBehaviour {

    #region PInvokes

    [DllImport("ros_interface_plugin", EntryPoint = "davinci_arm_create")]
    static private extern IntPtr CreateDaVinciArmInstance(string arm_name, string dvrk_namespace);

    [DllImport("ros_interface_plugin", EntryPoint = "davinci_arm_dispose")]
    static private extern void DisposeDaVinciArmInstance(IntPtr arm_ptr); 

    [DllImport("ros_interface_plugin", EntryPoint = "davinci_arm_has_messages")]
    static private extern byte HasMessagesImpl(IntPtr arm_ptr); 

    [DllImport("ros_interface_plugin", EntryPoint = "davinci_arm_get_namespace")]
    static private extern void GetArmNamespaceImpl(IntPtr arm_ptr, StringBuilder name);

    [DllImport("ros_interface_plugin", EntryPoint = "davinci_arm_get_joint_count")]
    static private extern int GetJointCountImpl(IntPtr arm_ptr);

    [DllImport("ros_interface_plugin", EntryPoint = "davinci_arm_get_joint_name")]
    static private extern byte GetJointNameImpl(IntPtr arm_ptr, int joint_number, StringBuilder name);

    [DllImport("ros_interface_plugin", EntryPoint = "davinci_arm_get_current_joint_position")]
    static private extern byte GetCurrentJointPositionImpl(IntPtr arm_ptr, int joint_number, out double joint_position);

    [DllImport("ros_interface_plugin", EntryPoint = "davinci_arm_get_desired_joint_position")]
    static private extern byte GetDesiredJointPositionImpl(IntPtr arm_ptr, int joint_number, out double joint_position);

    [DllImport("ros_interface_plugin", EntryPoint = "davinci_arm_get_current_joint_velocity")]
    static private extern byte GetCurrentJointVelocityImpl(IntPtr arm_ptr, int joint_number, out double joint_velocity);

    [DllImport("ros_interface_plugin", EntryPoint = "davinci_arm_get_desired_joint_velocity")]
    static private extern byte GetDesiredJointVelocityImpl(IntPtr arm_ptr, int joint_number, out double joint_velocity);

    [DllImport("ros_interface_plugin", EntryPoint = "davinci_arm_get_current_joint_effort")]
    static private extern byte GetCurrentJointEffortImpl(IntPtr arm_ptr, int joint_number, out double joint_effort);

    [DllImport("ros_interface_plugin", EntryPoint = "davinci_arm_get_desired_joint_effort")]
    static private extern byte GetDesiredJointEffortImpl(IntPtr arm_ptr, int joint_number, out double joint_effort);

    [DllImport("ros_interface_plugin", EntryPoint = "davinci_arm_get_current_position")]
    static private extern byte GetCurrentPositionImpl(IntPtr arm_ptr, out CartesianPosition position);

    [DllImport("ros_interface_plugin", EntryPoint = "davinci_arm_get_desired_position")]
    static private extern byte GetDesiredPositionImpl(IntPtr arm_ptr, out CartesianPosition position);

    #endregion

    #region Public Members

    public string ArmName;
    public string DVRKNamespace = "/dvrk/";

    public GameObject ArmProxy;

    public bool HasMessages {
        get {
            if (!_has_messages && _native_instance != IntPtr.Zero)
                _has_messages = (HasMessagesImpl(_native_instance) == 1);

            return _has_messages;
        }
    }

    public string ArmNamespace {
        get {
            if (string.IsNullOrEmpty(_arm_namespace))
                _arm_namespace = get_arm_namespace();    

            return _arm_namespace;
        }
    }

    public string[] JointNames {
        get {
            if (_joint_names == null || _joint_names.Length == 0) 
                get_joint_names();
            
            return _joint_names;
        }
    }

    public Vector3 CurrentPosition {
        get {
            if (_current_position_last_updated < Time.time)
                update_current_position();

            return _current_position;
        }
    }

    public Vector3 CurrentOrientation {
        get {
            if (_current_position_last_updated < Time.time)
                update_current_position();

            return _current_orientation;
        }
    }

    public Vector3 DesiredPosition {
        get {
            if (_desired_position_last_updated < Time.time)
                update_desired_position();

            return _desired_position;
        }
    }

    public Vector3 DesiredOrientation {
        get {
            if (_desired_position_last_updated < Time.time)
                update_desired_position();

            return _desired_orientation;
        }
    }

    #endregion

    #region Private Members

    private IntPtr _native_instance = IntPtr.Zero;

    private float _creation_timestamp = 0f;

    private bool _has_messages = false;

    private string _arm_namespace;
    private string[] _joint_names;

    private Vector3 _current_position;
    private Vector3 _current_orientation;
    private float _current_position_last_updated = 0f;

    private Vector3 _desired_position;
    private Vector3 _desired_orientation;
    private float _desired_position_last_updated = 0f;

    #endregion

    #region Callbacks

	// Use this for initialization
	void Awake () {
        if (string.IsNullOrEmpty(ArmName)) {
            throw new Exception("DaVinciCameraTexture::ArmName must not be empty");
        }

        if (string.IsNullOrEmpty(DVRKNamespace)) {
            throw new Exception("DaVinciCameraTexture::DVRKNamespace must not be empty");
        }

        this._native_instance = CreateDaVinciArmInstance(ArmName, DVRKNamespace);
        if (this._native_instance == IntPtr.Zero) {
            throw new Exception("Unable to create new DaVinciConsoleArm instance. Creation failed.");
        }

        this._creation_timestamp = Time.realtimeSinceStartup;        
        Debug.LogFormat("Created new DaVinciArm instance @ 0x{0:X8}", this._native_instance.ToInt64());
        

        StartCoroutine(CheckPoseMessages());
	}
	
	// Update is called once per frame
	void Update () {
        if (ArmProxy && HasMessages) {
            ArmProxy.transform.localPosition = CurrentPosition * 10;
            ArmProxy.transform.localEulerAngles = CurrentOrientation;
        }
	}

    void OnDestroy() {
        if (this._native_instance != IntPtr.Zero) {
            DisposeDaVinciArmInstance(this._native_instance);
            this._native_instance = IntPtr.Zero;
        }
    }

    #endregion

    public double GetJointPosition(int joint_number) {
        if (_native_instance == IntPtr.Zero) { 
            throw new InvalidOperationException();
        }
 
        double position = Double.NaN;
        if (GetCurrentJointPositionImpl(_native_instance, joint_number, out position) != 0) {
                Debug.LogErrorFormat(
                    "An error occurred whilst retreiving the current position of joint {0} on arm "
                    + "{1}. Consult the logs for more information", joint_number, ArmNamespace    
            );        
        }
 
        return position;
    }

    public double GetDesiredJointPosition(int joint_number) {
        if (_native_instance == IntPtr.Zero) { 
            throw new InvalidOperationException();
        }

        double position = Double.NaN;
        if (GetDesiredJointPositionImpl(_native_instance, joint_number, out position) != 0) {
                Debug.LogErrorFormat(
                    "An error occurred whilst retreiving the desired position of joint {0} on arm "
                    + "{1}. Consult the logs for more information", joint_number, ArmNamespace    
                );        
        }

        return position;
    }

    public double GetJointVelocity(int joint_number) {
        if (_native_instance == IntPtr.Zero) { 
            throw new InvalidOperationException();
        }

        double velocity = Double.NaN;
        if (GetCurrentJointVelocityImpl(_native_instance, joint_number, out velocity) != 0) {
                Debug.LogErrorFormat(
                    "An error occurred whilst retreiving the current velocity of joint {0} on arm "
                    + "{1}. Consult the logs for more information", joint_number, ArmNamespace    
                );        
        }

        return velocity;
    }

    public double GetDesiredJointVelocity(int joint_number) {
        if (_native_instance == IntPtr.Zero) { 
            throw new InvalidOperationException();
        }

        double velocity = Double.NaN;
        if (GetDesiredJointVelocityImpl(_native_instance, joint_number, out velocity) != 0) {
                Debug.LogErrorFormat(
                    "An error occurred whilst retreiving the desired velocity of joint {0} on arm "
                    + "{1}. Consult the logs for more information", joint_number, ArmNamespace    
                );        
        }

        return velocity;
    }

    public double GetJointEffort(int joint_number) {
        if (_native_instance == IntPtr.Zero) { 
            throw new InvalidOperationException();
        }

        double effort = Double.NaN;
        if (GetCurrentJointEffortImpl(_native_instance, joint_number, out effort) != 0) {
                Debug.LogErrorFormat(
                    "An error occurred whilst retreiving the current effort of joint {0} on arm "
                    + "{1}. Consult the logs for more information", joint_number, ArmNamespace    
                );        
        }

        return effort;
    }

    public double GetDesiredJointEffort(int joint_number) {
        if (_native_instance == IntPtr.Zero) { 
            throw new InvalidOperationException();
        }

        double effort = Double.NaN;
        if (GetDesiredJointEffortImpl(_native_instance, joint_number, out effort) != 0) {
                Debug.LogErrorFormat(
                    "An error occurred whilst retreiving the desired effort of joint {0} on arm "
                    + "{1}. Consult the logs for more information", joint_number, ArmNamespace    
                );        
        }

        return effort;
    }

    private string get_arm_namespace() {
        if (_native_instance == IntPtr.Zero) { 
            throw new InvalidOperationException();
        }
 
        StringBuilder arm_name = new StringBuilder(256);
        GetArmNamespaceImpl(_native_instance, arm_name);
 
        return arm_name.ToString();
    }
 
    private void get_joint_names() {
        if (_native_instance == IntPtr.Zero) { 
            throw new InvalidOperationException();
        }

        int joint_count = GetJointCountImpl(_native_instance);
        _joint_names = new string[joint_count];
        for (int joint_number = 0; joint_number < joint_count; ++joint_number) {
            StringBuilder joint_name = new StringBuilder(256);
         
            if (GetJointNameImpl(_native_instance, joint_number, joint_name) != 0) {
                Debug.LogErrorFormat(
                    "An error occurred whilst retreiving name for joint {0} on arm {1}. Consult "
                    + "the logs for more information", joint_number, ArmNamespace    
                );
            }

            _joint_names[joint_number] = joint_name.ToString();
        }
    }


    public void update_current_position() {
        if (_native_instance == IntPtr.Zero) { 
            throw new InvalidOperationException();
        }

        CartesianPosition position;
        if (GetCurrentPositionImpl(_native_instance, out position) != 0) {
            Debug.LogErrorFormat(
                    "An error occurred whilst retreiving the current position of arm {0}. Consult "
                    + " the logs for more information", ArmNamespace    
                );
        }

        _current_position = new Vector3(
            (float) position.pos_x, (float) position.pos_y, (float) position.pos_z);
        _current_orientation = new Vector3(
            (float) position.rot_x, (float) position.rot_y, (float) position.rot_z);
        _current_position_last_updated = Time.time;
    }

    public void update_desired_position() {
        if (_native_instance == IntPtr.Zero) { 
            throw new InvalidOperationException();
        }

        CartesianPosition position;
        if (GetDesiredPositionImpl(_native_instance, out position) != 0) {
            Debug.LogErrorFormat(
                    "An error occurred whilst retreiving the desired position of arm {0}. Consult "
                    + " the logs for more information", ArmNamespace    
                );
        }

        _desired_position = new Vector3(
            (float) position.pos_x, (float) position.pos_y, (float) position.pos_z);
        _desired_orientation = new Vector3(
            (float) position.rot_x, (float) position.rot_y, (float) position.rot_z);
        _desired_position_last_updated = Time.time;
    }

    #region CoRoutines 

    IEnumerator CheckPoseMessages() {
        if (_native_instance == IntPtr.Zero) {
            throw new InvalidOperationException();
        }

        while (!HasMessages) {
            float age = Time.realtimeSinceStartup - this._creation_timestamp;
            if (age > 1f) {
                Debug.LogWarningFormat("DaVinciArm has not recieved any messages after {0} seconds!", age);
            }

            yield return new WaitForSeconds(10f);
        }
    }

    #endregion


}
