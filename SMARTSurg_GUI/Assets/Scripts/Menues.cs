﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Menues : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    // panel for the different menues of the UI
    public GameObject Panel = null;
    

    // button to activate the different menues
    public Button Button = null;
    

    // colours and sizes to modify buttons
    private Color active_orange = new Color(1, 0.64F, 0, 1);
    private Color default_blue = new Color(0.098F, 0.7F, 0.69F, 1);
    private Vector3 zoomed = new Vector3(1.2F, 1.2F, 0);
    private Vector3 normal_size = new Vector3(1, 1, 0);

    // flag to see if a menu is open or closed
    private bool _panel_open = false;
    

    void Start() {
        Panel.SetActive(false);
        
        Button.onClick.AddListener(delegate {
            TogglePanel(ref Panel, ref Button, ref _panel_open); });
        
    }

    // change button's size when selected
    public void OnPointerEnter(PointerEventData eventData) {
        this.GetComponent<Image>().rectTransform.localScale = zoomed;
    }

    // chenge buttons' size when not selected anymore
    public void OnPointerExit(PointerEventData eventData) {
        if (!_panel_open) {
            Button.GetComponent<Image>().rectTransform.localScale = normal_size;
        }
    }

    // toggle menues
    public void TogglePanel(ref GameObject Pnl, ref Button Btn, ref bool _panel_open) {
        _panel_open = !_panel_open;
        Pnl.SetActive(_panel_open);
        if (_panel_open) {
            Btn.GetComponent<Image>().color = active_orange;
            Btn.GetComponent<Image>().rectTransform.localScale = zoomed;
        } else {
            Btn.GetComponent<Image>().color = default_blue;
            Btn.GetComponent<Image>().rectTransform.localScale = normal_size;
        }
    }
}
