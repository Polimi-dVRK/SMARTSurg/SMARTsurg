﻿using UnityEngine;
using System.Collections;

public enum RealityType {
    Augmented, 
    Virtual
}

public class DisplayManager : MonoBehaviour {

    #region Public Members 

    public RealityType RealityType; 

    public Camera MainCamera = null;
    public int MainCameraDisplay = 0;

    public Camera SurgeonConsoleLeftCamera = null;
    public int SurgeonConsoleLeftCameraDisplay = 1;

    public Camera SurgeonConsoleRightCamera = null;
    public int SurgeonConsoleRightCameraDisplay = 2;

    #endregion

    void Awake() {
        if (Application.isEditor) {
            Debug.Log(
                "Running application from within the editor. The displays count will always be 1. "
                + "To test the multi-monitor system compile and run the application standalone."
            );

            return;
        } 

        Display[] displays = Display.displays;
        if (displays.Length < 3) {
            Debug.LogErrorFormat(
                "At least 3 monitors are required to run this application (main monitor, "
                + "surgeon console left, surgeon console right) but only {0} monitors were "
                + "detected. The application will run with only the main display.", displays.Length
            );

            return;
        }

        var highest_requested_display = Mathf.Max(
            MainCameraDisplay, SurgeonConsoleLeftCameraDisplay, SurgeonConsoleRightCameraDisplay);
        if (displays.Length < highest_requested_display) {
            Debug.LogErrorFormat(
                "The application requested to connect to display #{0} but only {1} displays are "
                + " connected.", highest_requested_display, displays.Length
            );

            return;
        }


        if (!MainCamera) {
            Debug.LogError(
                "<MainCamera> field is unset. The application does not know which camera should "
                + "render to the main window. This may cause graphical issues."
            );
        } else {
            Debug.LogFormat("Binding camera <{0}> (Main) to display {1}", MainCamera.gameObject.name, MainCameraDisplay);
            MainCamera.targetDisplay = MainCameraDisplay;
        }

        if (!SurgeonConsoleLeftCamera) {
            Debug.LogError(
                "<SurgeonConsoleRightCamera> field is unset. The application does not know which "
                + "camera should render to the left monitor of the surgeon console. This may cause"
                + "graphical issues."
            );
        } else {
            Debug.LogFormat("Binding camera <{0}> (Surgeon Right) to display {1}", 
                SurgeonConsoleLeftCamera.gameObject.name, SurgeonConsoleLeftCameraDisplay);
            displays[SurgeonConsoleLeftCameraDisplay].Activate();
            SurgeonConsoleLeftCamera.targetDisplay = SurgeonConsoleLeftCameraDisplay;
        }

        if (!SurgeonConsoleRightCamera) {
            Debug.LogError(
                "<SurgeonConsoleRightCamera> field is unset. The application does not know which "
                + "camera should render to the right monitor of the surgeon console. This may cause"
                + "graphical issues."
            );
        } else {
            Debug.LogFormat("Binding camera <{0}> (Surgeon Left) to display {1}", 
                SurgeonConsoleRightCamera.gameObject.name, SurgeonConsoleRightCameraDisplay);
            displays[SurgeonConsoleRightCameraDisplay].Activate();
            SurgeonConsoleRightCamera.targetDisplay = SurgeonConsoleRightCameraDisplay;
        }
    }

}
