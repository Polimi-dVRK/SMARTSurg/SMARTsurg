﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;




public class DrawAC : MonoBehaviour {
	
	public Button DrawingButton = null;
	public RectTransform GuiCanvas = null;
	public Camera DrawingCamera = null;

	private bool _drawing_status = false;
	private LineRenderer _lineRenderer = null;
	List<Vector3> _region_point_list = new List<Vector3>();
	private Color _active_orange = new Color(1, 0.64F, 0, 1);
	private Color _default_blue = new Color(0.098F, 0.7F, 0.69F, 1);
	

	private GameObject _sphere_cursor = null;
	
	void Start() {
		DrawingButton.onClick.AddListener(delegate { change_drawing_status(); });
		// Get height and width off the GUI for coordinate normalization
		GuiCanvas = gameObject.GetComponent<RectTransform> ();
		Vector3[] v = new Vector3[4];
		GuiCanvas.ForceUpdateRectTransforms();
		GuiCanvas.GetWorldCorners(v);

		Debug.Log("World Corners");
		for (var i = 0; i < 4; i++) {
			Debug.Log("World Corner " + i + " : " + v[i]);
		}

		if (_sphere_cursor != null && _lineRenderer != null) {
			// Destroy(_sphere_cursor);
			Destroy(_lineRenderer);
		} else {
			_sphere_cursor = GameObject.CreatePrimitive(PrimitiveType.Sphere);
			// set line properties
			_lineRenderer = gameObject.AddComponent<LineRenderer>();
			_lineRenderer.material = new Material(Shader.Find("Particles/Alpha Blended"));
			_lineRenderer.startColor = _active_orange;
			_lineRenderer.endColor = _active_orange;
			_lineRenderer.widthMultiplier = 0.5f;
			// count the number of segments of the line
			_lineRenderer.positionCount = 0;
		}
	}
	
	void Update () {
		if (_drawing_status == true && !EventSystem.current.IsPointerOverGameObject()) {
			if (Input.GetMouseButton(0)) { 
				Vector3 mouse_position_current = DrawingCamera.ScreenToWorldPoint(
					new Vector3(Input.mousePosition.x, Input.mousePosition.y, 50));
				
				// _sphere_cursor.transform.position = mouse_position_current;
				_region_point_list.Add(mouse_position_current);
				
				// draw region
				Vector3[] region_point_array = _region_point_list.ToArray();
				_lineRenderer.positionCount = region_point_array.Length;
				_lineRenderer.SetPositions(region_point_array);
				// Debug.Log(mouse_position_current.ToString("F3"));
			}
		}
	}

	private void change_drawing_status() {
		_drawing_status = !_drawing_status;
		if (_drawing_status == true) {
			DrawingButton.GetComponent<Image>().color = _active_orange;
			// create new line emtying old one
			_lineRenderer.positionCount = 0;
			_region_point_list.Clear();
			_lineRenderer.loop = false;
		} else {
			DrawingButton.GetComponent<Image>().color = _default_blue;
			_lineRenderer.loop = true;
			// TODO: Save region and/or send it through ROS
		}
	}
	
	
}


