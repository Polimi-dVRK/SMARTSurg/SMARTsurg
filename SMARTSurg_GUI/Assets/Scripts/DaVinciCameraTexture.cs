﻿

using System;
using System.Collections;
using System.Runtime.InteropServices;

using UnityEngine;

public class DaVinciCameraTexture : MonoBehaviour {   

    #region Members

    public GameObject DaVinciConsole = null;

    public DaVinciCameraID SourceImageID = 0;

    private DaVinciConsole _davinci_console = null; 
    private Texture2D _my_texture = null;  

    #endregion

    void Awake () {
        if (!DaVinciConsole) {
            throw new Exception("DaVinciCameraTexture::DaVinciConsole must not be null");
        }

        _davinci_console = DaVinciConsole.GetComponent<DaVinciConsole>();
        if (!_davinci_console) {
            throw new Exception("Specified `DaVinciConsole` Game Object does not contain a `DaVinciConsole` script component");
        }

        _my_texture = new Texture2D(720, 576, TextureFormat.RGB24, false);
        _my_texture.filterMode = FilterMode.Point;
        _my_texture.Apply();

        var destination_material =  GetComponent<Renderer>().material;
        destination_material.mainTexture = _my_texture;
    }

    void Update(){     
        if (!_davinci_console) return; 

        _davinci_console.UpdateCameraTexture(_my_texture, SourceImageID);
    }
}
