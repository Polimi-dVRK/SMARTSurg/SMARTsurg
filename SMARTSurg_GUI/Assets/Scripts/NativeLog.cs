﻿
using System.Text;
using System.Runtime.InteropServices;

using UnityEngine;

public class NativeLog : MonoBehaviour {

    [DllImport("ros_interface_plugin", EntryPoint="GetLogEntry")]
    static private extern byte GetLogEntryImpl(StringBuilder message);

    private static string GetLogEntry() {
        StringBuilder message = new StringBuilder(2048);
        if (GetLogEntryImpl(message) == 0) {
            return message.ToString();
        }

        return null;
    }

    public void PrintNativeLogs() {
        while (true) {
            string message = GetLogEntry();
            if (string.IsNullOrEmpty(message))
                return;

            Debug.LogWarning(message);
        }
    }

    public void Update() {
        PrintNativeLogs();        
    }

}
