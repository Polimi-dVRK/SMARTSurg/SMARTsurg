# SMARTSurg GUI component

The GUI component of the SMARTSurg project is based on the Unity3D game engine. It provides an easy way to draw complex graphics in space. The engine uses C# as a scripting language but a robust _Native Plugin_ feature makes it possible to interact with unmanaged code. 


# Installation Instructions

## Unity3D Installation 

The system has been built with and tested with build #20180221 tested on Ubuntu 16.04 LTS.

To install Unity first download the _Linux Download Assistant_ from the Unity [website](https://beta.unity3d.com/download/ee2fb9f9da52/public_download.html) and execute it: 

    ./UnitSetup-20XX.Y.ZZZ

Follow the steps in the wizard to download Unity3D to the location of your choosing. 

To check for update and download the latest build go to the [Unity Forum thread on the Linux Editor](https://forum.unity.com/threads/unity-on-linux-release-notes-and-known-issues.350256/). The latest build will always be in the last post of the thread. 

## SMARTSurg GUI Download and Compilation 

The SMARTSurg GUI component (this repository) should be downloaded into a ROS workspace. To create a new workspace you must: 

1. Install ROS Kinetic (cf. [official documentation](http://wiki.ros.org/kinetic/Installation))
2. Download `python-catkin-tools`
    
        $ sudo apt install python-catkin-tools

2. Create the workspace
    
        $ cd $FOLDER_IN_WHICH_TO_CREATE_THE_WORKSPACE
        $ mkdir -p smartsurg_gui_workspace/src
        $ cd smartsurg_gui_workspace
        $ catkin init

3. Download the dependencies for the native plugin
    
        $ cd src
        $ git clone https://gitlab.com/Polimi-dVRK/dvrk/dvrk_common.git

4. Download this repository (the recursive flag also downloads the submodules).
    
        $ git clone --recursive https://gitlab.com/Polimi-dVRK/SMARTSurg/SMARTsurg.git

5. Build the native plugin. The build step also moves the plugin into place.
    
        $ catkin build 

You can now open Unity. At this stage however many things will not work. The appropriate dVRK ROS topics should be published. 

# GUI Functionalities 

The required GUI features and their status are listed here. 

### Functionalities' state

- {+ Buttons, toggles and sliders +}
    - **Preoperative images:**
        {+ preoperative images button +}, {+ transparency sliders +}, 
        {+ manual registration button +}, {+ advanced settings button +}
    - **Active constraint:** 
        {+ active constraint button +}, {+ visual AC toggle +},
        {+ actual AC toggle +}, {+ draw AC button +}
    - **Vitals:**
        {+ vitals button +}, {+ vitals settings button +}  
    - **Camera:**
        {+ camera button +}, {+ rec button +}, {+ zoom slider +},
        {+ move camera button +}

- {+ Interface initialization in ROS +}

- Startup menu
    - check ROS topic connections
    - check arm-world
    - arm-world calibration
    - check camera calibration
    - camera calibration

- {+ Multi monitor support +}

- {+ Display images from cameras in Unity interface +}

- {+ Display end effector position in Unity interface +}

- Draw AC (Active Constraint)
    - {+ draw AC on a plane with mouse +}
    - draw AC using robotic manipulator

- Import preoperative models
    - {+ upload preoperative obj models +}
    - {+ change model transparency +}
    - toggle rendering of uploaded models
    - drag model using robotic manipulator
    - zoom model using robotic manipulator

- Vital signs



# GUI I/O

### Input

- **Camera images:** the plugin subscribes to "/endoscope/left/raw/image_raw" and 
            "/endoscope/right/raw/image_raw" and receive "*sensor_msg::Image*" 
            images. 
            (The topics can be modified in "*robot_abstraction_class.cpp*" 
            NB: the plugin has to be recompiled if the source changes).
- **Opencv images:** if needed the plugin is also able to send opencv images to the GUI.






