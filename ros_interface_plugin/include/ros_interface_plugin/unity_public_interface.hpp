
/**
 * @file
 *
 * This file contains the definitions for the public interface our plugin exposes. This is not
 * strictly necessary for the plugin to work with Unity but makes it possible to use the functions
 * of the public interface in test programs.
 *
 * The definitions in this file pertain to the lifecyle of the plugin.
 *
 * The plugin is started when the Unity Editor is started and the `UnityPluginLoad` function is
 * called. This will only happen if we have at least one game object in the world that uses a
 * function that is part of the plugin. When the Editor shuts down the `UnitPluginUnload` function
 * is called.
 */


#pragma once

#include "Unity/IUnityInterface.h"

// Plugin Interface

extern "C" {

/**
 * This function is called by Unity when our plugin starts (game or editor launched). This is where
 * one time initialisation should be performed (logging, ros network, ...)
 *
 * @param unityInterfaces Unused
 */
void UnityPluginLoad(IUnityInterfaces *unityInterfaces);

/**
 * This function is called when the plugin is unloaded (when the game or the editor is shut down)
 * and should be used for cleanup. We don't have anything to clean up yet.
 */
void UnityPluginUnload();

char GetLogEntry(char* out);

}
