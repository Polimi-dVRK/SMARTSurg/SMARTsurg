//
// Created by tibo on 30/04/18.
//

#pragma once

#include "ros_interface_plugin/dvrk/davinci_arm.hpp"

/**
 * In C++ the size of a bool is implementation defined and may be `sizeof(int)`, which is 4 bytes.
 * The CLR expects bools to be 1 byte we can't use them directly.
 *
 * Here is the workaround, use an ExitStatus enum that fits in a char (1 byte). This also gives us
 * room to express more errors if we ever need it.
 *
 * Note: this uses the UNIX style 0-is-success convention.
 */
enum ExitStatus: char {
    Success = 0,
    Failure = 1,
    NoData
};


struct CartesianPosition {
    
    static  CartesianPosition FromKDLFrame(const KDL::Frame &src);
    
    double pos_x = -1., pos_y = -1., pos_z = -1.;
    double rot_x = -1., rot_y = -1., rot_z = -1.;
};

extern "C" {
    
    

    dvrk::DaVinciArm* davinci_arm_create(
        const char* arm_name, const char* dvrk_namespace);

    void davinci_arm_dispose(dvrk::DaVinciArm* instance);
    
    char davinci_arm_has_messages(dvrk::DaVinciArm* instance);
    
    // TODO: add status checks to these methods
    void davinci_arm_get_name(const dvrk::DaVinciArm* instance, char* out);
    void davinci_arm_get_namespace(const dvrk::DaVinciArm* instance, char* out);
    
    char davinci_arm_get_current_state(const dvrk::DaVinciArm* instance, char* out);
    char davinci_arm_get_desired_state(const dvrk::DaVinciArm* instance, char* out);
    
    int davinci_arm_get_joint_count(const dvrk::DaVinciArm* instance);
    
    char davinci_arm_get_joint_name(
        const dvrk::DaVinciArm* instance, int joint_number, char* out);
    
    char davinci_arm_get_current_joint_position(
        const dvrk::DaVinciArm* instance, int joint_number, double* out);
    
    char davinci_arm_get_current_joint_velocity(
        const dvrk::DaVinciArm* instance, int joint_number, double* out);
    
    char davinci_arm_get_current_joint_effort(
        const dvrk::DaVinciArm* instance, int joint_number, double* out);

    char davinci_arm_get_desired_joint_position(
        const dvrk::DaVinciArm* instance, int joint_number, double* out);
    
    char davinci_arm_get_desired_joint_velocity(
        const dvrk::DaVinciArm* instance, int joint_number, double* out);
    
    char davinci_arm_get_desired_joint_effort(
        const dvrk::DaVinciArm* instance, int joint_number, double* out);

    char davinci_arm_get_current_position(dvrk::DaVinciArm* instance, CartesianPosition* out);

    char davinci_arm_get_desired_position(dvrk::DaVinciArm* instance, CartesianPosition* out);

}
