//
// Created by tibo on 30/04/18.
//

#pragma once

#include <string>
#include <GL/glew.h>

namespace dvrk {
    
    bool check_gl_ok();
    
    const std::string gl_enum_to_string(GLenum glvalue);
}
