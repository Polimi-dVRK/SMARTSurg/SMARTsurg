
/**
 * @file
 *
 * This file contains the definitions for the public interface our plugin exposes. This is not
 * strictly necessary for the plugin to work with Unity but makes it possible to use the functions
 * of the public interface in test programs.
 *
 * The definitions in this file pertain to the public API used to interface the dvrk::DaVinciRobot
 * class with the Unity game engine.
 */


#pragma once

#include "ros_interface_plugin/dvrk/davinci_arm.hpp"

// Forward declarations

namespace dvrk {
    class DaVinciRobot;
}

struct Position {
    double x = 0., y = 0., z = 0.;
};


// Plugin Interface

extern "C" {
    
    /**
     * Check whether roscore is running.
     * @return True if the core is running and responding, false otherwise
     */
    bool ros_check() {
        return ros::master::check();
    }
    
    /**
     * Create an instance of a dvrk::DaVinciRobot.
     *
     * @return The pointer to the newly created instance
     */
    dvrk::DaVinciRobot *davinci_console_create();
    
    /*
     * Destroy a previously constructed dvrk::DaVinciRobot
     */
    void davinci_console_dispose(dvrk::DaVinciRobot *instance);
    
    /**
     * Subscribe to the camera topics of a DaVinciRobot
     *
     *  @param instance The instance on which to operate
     */
    void davinci_console_setup_cameras(dvrk::DaVinciRobot *instance);
    
    /**
     * Has the endoscope received an image yet ?
     *  @param instance The instance on which to operate
     * @return Whether or not the endoscope has received an image
     */
    bool davinci_console_endoscope_has_images(dvrk::DaVinciRobot *instance);
    
    /**
     * How many stereo pairs has the endoscope received ?
     *  @param instance The instance on which to operate
     * @return The number of image pairs received since the cameras were subsribed to
     */
    uint32_t davinci_console_get_endoscope_image_count(dvrk::DaVinciRobot *instance);
    
    double davinci_console_left_image_timestamp(dvrk::DaVinciRobot *instance);

    void davinci_console_update_texture(
        dvrk::DaVinciRobot *robot,
        void *texture_handle,
        int texture_width,
        int texture_height,
        int source_image_id
    );
 
}
