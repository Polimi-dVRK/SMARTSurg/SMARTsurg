//
// Created by tibo on 03/05/18.
//

#pragma once

#include <deque>
#include <mutex>

#include <plog/Log.h>

#include <boost/optional.hpp>

/**
 *
 * @note This code assumes that it will only ever be run on linux. On windows platforms the plog
 * underlying string type defaults to std::wstring which will break a lot of things.
 *
 * @tparam Formatter
 */
template <class Formatter>
class UnityAppender : public plog::IAppender {
public:
    virtual void write(const plog::Record &record) {
        // The severity levels are backwards 1 is fatal, 6 is verbose. We want only logs with
        // severity 3 (warning) and lower.
        if (record.getSeverity() > plog::Severity::warning)
            return;
        
        std::lock_guard<std::mutex> lock_messages(_messages_lock);
        
        std::string message = Formatter::format(record);
        _messages.push_back(std::string("[Native Plugin] ") + message);
    }
    
    boost::optional<std::string> pop_message() {
        std::lock_guard<std::mutex> lock_messages(_messages_lock);
        
        if (_messages.empty())
            return boost::none;
        
        std::string message = _messages.front();
        _messages.pop_front();
        
        return message;
    }
    
private:
    
    std::mutex _messages_lock;
    std::deque<plog::util::nstring> _messages;
};
