//
// Created by Simone Foti on 9/3/18.
//

#pragma once

#include <ros/ros.h>

#include "dvrk_common/ros/camera/stereo_camera.hpp"

namespace dvrk {
    
    enum class DaVinciCameraID {
        None = 0,
        EndoscopeLeft = 1,
        EndoscopeRight = 2
    };
    
    class DaVinciRobot {
    
    public: /* Public Methods - Initialisation */
        DaVinciRobot(ros::NodeHandle nh);
        
        void setup_cameras();
        
        
    public: /* Public Methods - Endoscope Interface */
        
        /**
         * Set the callback function called each time an image is received to the specified function object.
         *
         * @param cb The callback function that will be called each time an image is received.
         */
        void setOnImageCallback(dvrk::StereoCamera::image_cb_t cb);
    
        template <class T>
        void setOnImageCallback(dvrk::StereoCamera::image_cb_method_t<T> cb, T *owner);
        
        bool wait_for_first_image() const;
        
        bool endoscope_has_images() const;
        
        uint32_t get_endoscope_image_count() const;
        
        const dvrk::StereoImage get_stereo_image() const;
        
        const sensor_msgs::ImageConstPtr get_left_image() const;
        
        const sensor_msgs::ImageConstPtr get_right_image() const;
        
    
    public:
        
        const std::string left_camera_topic = "/endoscope/raw/left/image_raw";
        const std::string right_camera_topic = "/endoscope/raw/right/image_raw";
    
    private:
        
        /* ROS Stuff */
        
        ros::NodeHandle _nh;
        
        /* Console Bits and Pieces */
        
        dvrk::StereoCamera _stereo_camera;
    };
    
    
    /**
     * Set the callback function called each time a new image is received to the specified member function \p cb of the
     * instance of \p T passed in as \p owner.
     *
     * @tparam T The type of the class to which the specified callback belongs.
     * @param cb The callback function to call each time an image is received.
     * @param owner The class instance on which to call the callback.
     */
    template <class T>
    void DaVinciRobot::setOnImageCallback(dvrk::StereoCamera::image_cb_method_t<T> cb, T* owner) {
        _stereo_camera.set_on_image_callback(std::move(cb), owner);
    }
}
