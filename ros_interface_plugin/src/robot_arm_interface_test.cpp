//
// Created by tibo on 26/03/18.
//

#include <sstream>
#include <plog/Log.h>

#include <ros_interface_plugin/unity_public_interface.hpp>
#include <ros_interface_plugin/davinci_arm_interface.hpp>
#include <ros_interface_plugin/dvrk/davinci_arm.hpp>

using namespace dvrk;

std::string str_or_placeholder(char* str, std::string placeholder) {
    return strlen(str) == 0 ? placeholder : std::string(str);
}

int main() {
    
    UnityPluginLoad(nullptr);
    
    LOG_INFO << "";
    
    // DaVinciArm* arm = davinci_arm_create("MTML", "/dvrk/");
    ros::NodeHandle nh;
    DaVinciArm *arm = new DaVinciArm(nh, "MTML", "/dvrk/");
    
    LOG_INFO << "Created DaVinci arm wrapper (" << arm << ")";
    LOG_WARNING_IF(!arm) << "Unable to create daVinci arm wrapper";
    
    LOG_INFO << "Spinning until we see the joints appear or timeout...";
    
    ros::Time loop_exit = ros::Time::now() + ros::Duration(2.0);
    ros::Rate spin_rate(100);
    while (ros::ok() and ros::Time::now() < loop_exit) {
        ros::spinOnce();
        spin_rate.sleep();
    
        const auto joint_count = davinci_arm_get_joint_count(arm);
        if (joint_count > 0) break;
    }
    
    char arm_name[256], arm_namespace[256];
    davinci_arm_get_name(arm, arm_name);
    davinci_arm_get_namespace(arm, arm_namespace);
    
    std::ostringstream oss;
    
    oss << "\nArm " << str_or_placeholder(arm_name, "Unkown Arm")
        << "(namespace: " << str_or_placeholder(arm_namespace, "N/A") << ")\n";
    
    char current_state[256], desired_state[256];
    davinci_arm_get_current_state(arm, current_state);
    davinci_arm_get_desired_state(arm, desired_state);
    
    oss << "   - State: " << str_or_placeholder(current_state, "Unknown") << " (desired: "
        << str_or_placeholder(desired_state, "Unknown") << ")\n";
    
    const auto joint_count = davinci_arm_get_joint_count(arm);
    oss << "   - Joints (" << joint_count << "): \n";
    
    for (auto i = 0; i < joint_count; ++i) {
        char joint_name[256];
        if (davinci_arm_get_joint_name(arm, i, joint_name) != 0) {
            LOG_WARNING << "Unable to retrieve joint name for joint #" << i << " on arm " << arm_name;
            strcpy(joint_name, "Unknown");
        }
        
        double position;
        if (davinci_arm_get_current_joint_position(arm, i, &position) != 0) {
            LOG_WARNING << "Unable to retrieve joint current position for joint #" << i << " on arm " << arm_name;
            position = 0.0;
        }
        
        oss << "     #" << i << " (" <<  joint_name << ") - pos: " << position << "\n";
    }
    
    oss << "\n"
        << "Topics: \n"
        << "  Current State:       " << arm->get_current_state_topic() << "\n"
        << "  Desired State:       " << arm->get_desired_state_topic() << "\n"
        << "  Goal Reached:        " << arm->get_goal_reached_topic() << "\n"
        << "  Current Joint State: " << arm->get_current_joint_state_topic() << "\n";
    
    LOG_INFO << oss.str();
    
    UnityPluginUnload();
    
    return 0;
}
