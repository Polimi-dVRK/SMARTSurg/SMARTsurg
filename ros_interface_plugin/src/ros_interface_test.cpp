//
// Created by tibo on 26/03/18.
//

#include <cassert>

#include <ros_interface_plugin/dvrk/davinci_console.hpp>
#include <ros_interface_plugin/unity_public_interface.hpp>
#include <ros_interface_plugin/davinci_console_interface.hpp>

int main() {
    
    UnityPluginLoad(nullptr);
    
    dvrk::DaVinciRobot* robot = davinci_console_create();
    assert(robot != nullptr);
    
    davinci_console_setup_cameras(robot);
    
    std::cout << "Spinning for 2 seconds ...\n";
    
    ros::Rate spin_rate(100);
    ros::WallTime loop_exit_time = ros::WallTime::now() + ros::WallDuration(2, 0);
    
    while (ros::ok() and ros::WallTime::now() < loop_exit_time) {
        ros::spinOnce();
        spin_rate.sleep();
    }
    
    const auto pair = robot->get_stereo_image();
    
    std::cout << ros::Time::now() << " - Got a first pair of images with left-seq# = " << pair.left->header.seq
              << ", right-seq# = " << pair.right->header.seq << "\n";
    
    std::cout << "Spinning for 2 seconds ...\n";
    
    loop_exit_time = ros::WallTime::now() + ros::WallDuration(2, 0);
    while (ros::ok() and ros::WallTime::now() < loop_exit_time) {
        ros::spinOnce();
        spin_rate.sleep();
    }
    
    std::cout << ros::Time::now() << " - Got a new pair of images with left-seq# = " << pair.left->header.seq
              << ", right-seq# = " << pair.right->header.seq << "\n";
    
    davinci_console_dispose(robot);
    robot = nullptr;
    
    UnityPluginUnload();
    
    return 0;
}
