//
// Created by tibo on 30/04/18.
//

#include <plog/Log.h>

#include "ros_interface_plugin/davinci_arm_interface.hpp"


CartesianPosition CartesianPosition::FromKDLFrame(const KDL::Frame& src) {
    CartesianPosition dst;
    dst.pos_x = src.p.x();
    dst.pos_y = src.p.y();
    dst.pos_z = src.p.z();
    
    const auto rotation = src.M.GetRot();
    dst.rot_x = rotation.x();
    dst.rot_y = rotation.y();
    dst.rot_z = rotation.z();
    
    return dst;
}


bool check_joint_number(const dvrk::DaVinciArm* instance, int joint_number) {
    const auto joint_count = instance->get_joint_count();
    if (joint_number < 0 or static_cast<size_t>(joint_number) > joint_count - 1) {
        LOG_ERROR << "Requested joint #" << joint_number << " on arm "
                  << instance->get_namespace() << " but there are only "
                  << instance->get_joint_count() << " joints on this arm.";
        return false;
    }
    
    return true;
}

extern "C" {
    
    dvrk::DaVinciArm *davinci_arm_create(const char *arm_name, const char *dvrk_namespace) {
        if (!arm_name or strlen(arm_name) == 0) {
            LOG_WARNING << "Invalid arm name. `arm_name` should be a non-empty string";
            return nullptr;
        }
        
        if (!dvrk_namespace or strlen(dvrk_namespace) == 0) {
            LOG_WARNING << "Invalid arm name. `dvrk_namespace` should be a non-empty string";
            return nullptr;
        }
        
        LOG_INFO << "Setting up davinci arm class ...";
        return new dvrk::DaVinciArm(arm_name, dvrk_namespace);
    }
    
    void davinci_arm_dispose(dvrk::DaVinciArm *instance) {
        if (instance == nullptr) {
            LOG_WARNING << "Called `davinci_arm_dispose` with a null pointer.";
            return;
        }
        
        delete instance;
    }

    char davinci_arm_has_messages(dvrk::DaVinciArm *instance) {
        if (instance == nullptr) {
            LOG_WARNING << "Called `davinci_arm_has_messages` with a null pointer.";
            return 0;
        }
        
        return static_cast<char>(instance->has_messages());
    }
    
    char davinci_arm_get_current_state(const dvrk::DaVinciArm *instance, char *out) {
        if (instance == nullptr) {
            LOG_WARNING << "Called `davinci_arm_dispose` with a null pointer.";
            return ExitStatus::Failure;
        }
        
        const auto result = instance->get_current_state();
        if (!result) {
            LOG_WARNING << "Called `davinci_arm_get_current_state` before any data was available.";
            return ExitStatus::NoData;
        }
        
        std::string current_state = *result;
        strncpy(out, current_state.c_str(), 256);
        
        return ExitStatus::Success;
    }
    
    char davinci_arm_get_desired_state(const dvrk::DaVinciArm *instance, char *out) {
        if (instance == nullptr) {
            LOG_WARNING << "Called `davinci_arm_dispose` with a null pointer.";
            return ExitStatus::Failure;
        }
    
        const auto result = instance->get_desired_state();
        if (!result) {
            LOG_WARNING << "Called `davinci_arm_get_desired_state` before any data was available.";
            return ExitStatus::NoData;
        }
    
        std::string desired_state = *result;
        strncpy(out, desired_state.c_str(), 256);
    
        return ExitStatus::Success;
    }
    
    void davinci_arm_get_name(const dvrk::DaVinciArm *instance, char *out) {
        if (instance == nullptr) {
            LOG_WARNING << "Called `davinci_arm_get_name` with a null pointer.";
            return;
        }
        
        std::string arm_name = instance->get_name();
        strncpy(out, arm_name.c_str(), 256);
    }
    
    void davinci_arm_get_namespace(const dvrk::DaVinciArm *instance, char *out) {
        if (instance == nullptr) {
            LOG_WARNING << "Called `davinci_arm_get_namespace` with a null pointer.";
            return;
        }
        
        std::string arm_namespace = instance->get_namespace();
        strncpy(out, arm_namespace.c_str(), 256);
    }

    int davinci_arm_get_joint_count(const dvrk::DaVinciArm *instance) {
        if (instance == nullptr) {
            LOG_WARNING << "Called `davinci_arm_get_joint_count` with a null pointer.";
            return -1;
        }
     
        return static_cast<int>(instance->get_joint_names().size());
    }

    char davinci_arm_get_joint_name(const dvrk::DaVinciArm *instance, int joint_number, char *out) {
        if (instance == nullptr) {
            LOG_WARNING << "Called `davinci_arm_get_joint_name` with a null pointer.";
            return ExitStatus::Failure;
        }
        
        if (!check_joint_number(instance, joint_number))
            return ExitStatus::Failure;
        
        const auto result = instance->get_joint_name(joint_number);
        if (!result) {
            LOG_WARNING << "Called `davinci_arm_get_joint_name` before any data was available.";
            return ExitStatus::NoData;
        }
        
        std::string joint_name = *result;
        strncpy(out, joint_name.c_str(), 256);
        return ExitStatus::Success;
    }

    char davinci_arm_get_current_joint_position(
        const dvrk::DaVinciArm *instance, int joint_number, double *out
    ) {
        if (instance == nullptr) {
            LOG_WARNING << "Called `davinci_arm_get_current_joint_position` with a null pointer.";
            return ExitStatus::Failure;
        }
        
        if (!check_joint_number(instance, joint_number))
            return ExitStatus::Failure;
    
        const auto result = instance->get_current_joint_position(joint_number);
        if (!result) {
            LOG_WARNING << "Called `davinci_arm_get_current_joint_position` before any data was available.";
            return ExitStatus::NoData;
        }
    
        *out = *result;
        return ExitStatus::Success;
    }

    char davinci_arm_get_current_joint_velocity(
        const dvrk::DaVinciArm *instance, int joint_number, double *out
    ) {
        if (instance == nullptr) {
            LOG_WARNING << "Called `davinci_arm_get_current_joint_velocity` with a null pointer.";
            return ExitStatus::Failure;
        }
        
        if (!check_joint_number(instance, joint_number))
            return ExitStatus::Failure;
    
        const auto result = instance->get_current_joint_velocity(joint_number);
        if (!result) {
            LOG_WARNING << "Called `davinci_arm_get_current_joint_velocity` before any data was available.";
            return ExitStatus::NoData;
        }
    
        *out = *result;
        return ExitStatus::Success;
    }

    char davinci_arm_get_current_joint_effort(
        const dvrk::DaVinciArm *instance, int joint_number, double *out
    ) {
        if (instance == nullptr) {
            LOG_WARNING << "Called `davinci_arm_get_current_joint_effort` with a null pointer.";
            return ExitStatus::Failure;
        }
        
        if (!check_joint_number(instance, joint_number))
            return ExitStatus::Failure;
    
        const auto result = instance->get_current_joint_effort(joint_number);
        if (!result) {
            LOG_WARNING << "Called `davinci_arm_get_current_joint_effort` before any data was available.";
            return ExitStatus::NoData;
        }
    
        *out = *result;
        return ExitStatus::Success;
    }

    char davinci_arm_get_desired_joint_position(
        const dvrk::DaVinciArm *instance, int joint_number, double *out
    ) {
        if (instance == nullptr) {
            LOG_WARNING << "Called `davinci_arm_get_desired_joint_position` with a null pointer.";
            return ExitStatus::Failure;
        }
        
        if (!check_joint_number(instance, joint_number))
            return ExitStatus::Failure;
    
        const auto result = instance->get_desired_joint_position(joint_number);
        if (!result) {
            LOG_WARNING << "Called `davinci_arm_get_desired_joint_position` before any data was available.";
            return ExitStatus::NoData;
        }
    
        *out = *result;
        return ExitStatus::Success;
    }
    
    char davinci_arm_get_desired_joint_velocity(
        const dvrk::DaVinciArm *instance, int joint_number, double *out
    ) {
        if (instance == nullptr) {
            LOG_WARNING << "Called `davinci_arm_get_desired_joint_velocity` with a null pointer.";
            return ExitStatus::Failure;
        }
        
        if (!check_joint_number(instance, joint_number))
            return ExitStatus::Failure;
    
        const auto result = instance->get_desired_joint_velocity(joint_number);
        if (!result) {
            LOG_WARNING << "Called `davinci_arm_get_desired_joint_velocity` before any data was available.";
            return ExitStatus::NoData;
        }
    
        *out = *result;
        return ExitStatus::Success;
    }
    
    char davinci_arm_get_desired_joint_effort(
        const dvrk::DaVinciArm *instance, int joint_number, double *out
    ) {
        if (instance == nullptr) {
            LOG_WARNING << "Called `davinci_arm_get_desired_joint_effort` with a null pointer.";
            return ExitStatus::Failure;
        }
        
        if (!check_joint_number(instance, joint_number))
            return ExitStatus::Failure;
        
        const auto result = instance->get_desired_joint_effort(joint_number);
        if (!result) {
            LOG_WARNING << "Called `davinci_arm_get_desired_joint_effort` before any data was available.";
            return ExitStatus::NoData;
        }
        
        *out = *result;
        return ExitStatus::Success;
    }


    char davinci_arm_get_current_position(dvrk::DaVinciArm *instance, CartesianPosition *out) {
        if (instance == nullptr) {
            LOG_WARNING << "Called `davinci_arm_get_current_position` with a null pointer.";
            return ExitStatus::Failure;
        }
        
        const auto position = instance->get_current_position();
        if (!position) {
            LOG_WARNING << "Called `davinci_arm_get_current_position` before any data was available.";
            return ExitStatus::NoData;
        }
        *out = CartesianPosition::FromKDLFrame(*position);
        return ExitStatus::Success;
    }

    char davinci_arm_get_desired_position(dvrk::DaVinciArm *instance, CartesianPosition *out) {
        if (instance == nullptr) {
            LOG_WARNING << "Called `davinci_arm_get_desired_position` with a null pointer.";
            return ExitStatus::Failure;
        }
        
        const auto position = instance->get_desired_position();
        if (!position) {
            LOG_WARNING << "Called `davinci_arm_get_desired_position` before any data was available.";
            return ExitStatus::NoData;
        }
        
        *out = CartesianPosition::FromKDLFrame(*position);
        
        return ExitStatus::Success;
    }

}





