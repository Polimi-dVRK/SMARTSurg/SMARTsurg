//
// Created by tibo on 06/04/18.
//

//
// Created by tibo on 29/03/18.
//

#include <stdexcept>

#include <plog/Log.h>
#include <plog/Appenders/ConsoleAppender.h>

#include <GL/glew.h>

#include <sensor_msgs/image_encodings.h>
#include <dvrk_common/ros/camera/camera_errors.hpp>

#include <ros_interface_plugin/opengl/debug.hpp>
#include <ros_interface_plugin/davinci_console_interface.hpp>
#include <ros_interface_plugin/dvrk/davinci_console.hpp>

GLenum ros_image_encoding_to_gl_format(const std::string& encoding) {
    namespace enc = sensor_msgs::image_encodings;
    
    if (encoding == enc::BGR8) {
        return GL_BGR;
    } else if (encoding == enc::BGRA8) {
        return GL_BGRA;
    } else {
        return GL_INVALID_VALUE;
    }
}

extern "C" {

void davinci_console_update_texture(
    dvrk::DaVinciRobot *robot,
    void *texture_handle,
    int texture_width,
    int texture_height,
    int source_image_id
) {
    if (!robot) {
        LOG_ERROR << "Texture handle was null";
        return;
    }
    
    if (!texture_handle) {
        LOG_ERROR << "Texture handle was null";
        return;
    }
    
    if (texture_width < 0 or texture_height < 0) {
        LOG_ERROR << "Invalid texture size, width and height must be positive. They were: <w = "
                  << texture_width << ", h = " << texture_height << ">";
        return;
    }
    
    sensor_msgs::ImageConstPtr source_image;
    const auto source_camera = static_cast<dvrk::DaVinciCameraID>(source_image_id);
    switch (source_camera) {
        case dvrk::DaVinciCameraID::None:
            return;
        
        case dvrk::DaVinciCameraID::EndoscopeLeft:
            source_image = robot->get_left_image();
            break;
        
        case dvrk::DaVinciCameraID::EndoscopeRight:
            source_image = robot->get_right_image();
            break;
        
        default:
            LOG_ERROR << "Invalid source image ID: <source_image_id = " << source_image_id << ">";
            return;
    }
    
    if (source_image->data.empty()) {
        LOG_WARNING << "Source image was empty. maybe no images have been received yet ?";
        return;
    }
    
    if (source_image->width != static_cast<uint32_t>(texture_width)) {
        LOG_ERROR << "Expected texture width (" << texture_width << " px) and actual texture width ("
                  << source_image->width << " px) do not match. I cannot proceed";
        return;
    }
    
    if (source_image->height != static_cast<uint32_t>(texture_height)) {
        LOG_ERROR << "Expected texture height (" << texture_height << " px) and actual texture height ("
                  << source_image->height << " px) do not match. I cannot proceed";
        return;
    }
    
    GLenum texture_format = ros_image_encoding_to_gl_format(source_image->encoding);
    if (texture_format != GL_BGR) {
        LOG_ERROR << "Source texture has unhandled image encoding <" << source_image->encoding
                  << " (GL: " << dvrk::gl_enum_to_string(texture_format) << " >";
        return;
    }
    
    const int data_size = texture_width * texture_height * 3;
    if (source_image->data.size() != static_cast<unsigned int>(data_size)) {
        LOG_ERROR << "Expected data size (" << data_size << " bytes) and actual data size ("
                  << source_image->data.size() << " bytes) do not match up. I Cannot proceed.";
        return;
    }
    
    // TODO: Check that the image is uint8_t and not higher precision.
    
    auto gltex = (GLuint) (size_t) (texture_handle);
    glBindTexture(GL_TEXTURE_2D, gltex);
    if (!dvrk::check_gl_ok()) return;
    
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0,
        texture_width, texture_height, texture_format, GL_UNSIGNED_BYTE, source_image->data.data()
    );
    if (!dvrk::check_gl_ok()) return;
}

dvrk::DaVinciRobot *davinci_console_create() {
    return new dvrk::DaVinciRobot(ros::NodeHandle());
}

void davinci_console_dispose(dvrk::DaVinciRobot *instance) {
    if (instance == nullptr) {
        LOG_WARNING << "Called `davinci_console_dispose` with a null pointer.";
        return;
    }
    
    delete instance;
}

void davinci_console_setup_cameras(dvrk::DaVinciRobot *instance) {
    if (instance == nullptr) {
        LOG_WARNING << "Called `davinci_console_setup_cameras` with a null pointer.";
        return;
    }
    
    instance->setup_cameras();
}

double davinci_console_left_image_timestamp(dvrk::DaVinciRobot *instance) {
    if (instance == nullptr) {
        LOG_WARNING << "Called `davinci_console_left_image_timestamp` with a null pointer.";
        return -1.0;
    }
    
    try {
        const sensor_msgs::ImageConstPtr image_msg = instance->get_left_image();
        return image_msg->header.stamp.toSec();
        
    } catch (const dvrk::error::NoImageReceivedError& ex) {
        LOG_ERROR << "Called `dvrk::StereoCamera::getLeftImage()` but there were no images";
    }
    
    return -1.0;
}

bool davinci_console_endoscope_has_images(dvrk::DaVinciRobot *instance) {
    if (instance == nullptr) {
        LOG_WARNING << "Called `davinci_console_endoscope_has_images` with a null pointer.";
        return false;
    }
    
    return instance->endoscope_has_images();
}

uint32_t davinci_console_get_endoscope_image_count(dvrk::DaVinciRobot *instance) {
    if (instance == nullptr) {
        LOG_WARNING << "Called `davinci_console_get_endoscope_image_count` with a null pointer.";
        return 0;
    }
    
    return instance->get_endoscope_image_count();
}



}
