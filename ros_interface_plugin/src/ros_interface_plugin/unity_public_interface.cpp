//
// Created by tibo on 29/03/18.
//

#include <stdexcept>

#include <ros/ros.h>
#include <ros/spinner.h>

#include <plog/Log.h>
#include <plog/Appenders/ConsoleAppender.h>
#include <plog/Formatters/FuncMessageFormatter.h>

#include <ros_interface_plugin/plog_unity_appender.hpp>
#include <ros_interface_plugin/unity_public_interface.hpp>

std::unique_ptr<ros::AsyncSpinner> g_async_spinner;

using UnityAppender_t = UnityAppender<plog::FuncMessageFormatter>;
std::unique_ptr<UnityAppender_t> g_unity_appender;

extern "C" {

/**
* This method is called by Unity during startup when the plugin is loaded.
*
* Use it for one-time construction of things that must exist for the entire lifetime of the plugin
*/
void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API UnityPluginLoad(
    IUnityInterfaces * /* unityInterfaces - Unused */
) {
    // using ConsoleAppender = plog::ConsoleAppender<plog::TxtFormatter>;
    using FileAppender = plog::RollingFileAppender<plog::TxtFormatter>;
    
    // static ConsoleAppender console_appender;
    g_unity_appender = std::make_unique<UnityAppender_t>();
    static FileAppender file_appender("libros_interface_plugin.log", 8000, 1);
    plog::init(plog::debug, g_unity_appender.get()).addAppender(&file_appender);
    
    LOG_INFO << "";
    LOG_INFO << "######################################################################";
    LOG_INFO << "##                                                                  ##";
    LOG_INFO << "##                       ROS Interface Plugin                       ##";
    LOG_INFO << "##                                                                  ##";
    LOG_INFO << "######################################################################";
    LOG_INFO << "";
    LOG_INFO << "ROS interface plugin starting ...";
    LOG_INFO << "Initialising ROS ...";
    
    // Set the retry timeout really low so that we will fail immediately if there is a problem
    // instead of just waiting around for a long time.
    ros::master::setRetryTimeout(ros::WallDuration(0.1));
    
    LOG_DEBUG << "ROS Master seems to be up, initialising node ...";
    ros::M_string remappings;
    ros::init(remappings, "ros_interface");
    
    LOG_DEBUG << "Node up, starting up spinner and setting parameters ...";
    g_async_spinner = std::make_unique<ros::AsyncSpinner>(1);
    g_async_spinner->start();
    
    LOG_DEBUG << "Setting image transport compression scheme to <theora>";
    ros::NodeHandle pnh("~");
    pnh.setParam("image_transport", "theora");
    
    LOG_ERROR_IF(!ros::master::check() or !ros::ok())
        << "It looks like something went wrong when setting up ROS.";
    
    LOG_INFO << "All done, node up and running.";
}

/**
 * This function is called by Unity when the plugin is unloaded.
 */
void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API UnityPluginUnload() {
    LOG_INFO << "Shutting down ROS ...";
    g_async_spinner->stop();
    ros::requestShutdown();
    
    LOG_INFO << "plugin shutting down cleanly. Goodbye.";
}


char GetLogEntry(char *out) {
    if (!g_unity_appender) {
        return 1;
    }
    
    const auto retval = g_unity_appender->pop_message();
    std::string message = boost::get_optional_value_or(retval, "");
    strncpy(out, message.c_str(), 2048);
    
    return 0;
}

}
