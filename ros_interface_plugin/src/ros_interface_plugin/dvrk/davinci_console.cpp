//
// Created by Simone Foti on 9/3/18.
//

#include <plog/Log.h>
#include <ros/package.h>

#include "ros_interface_plugin/dvrk/davinci_console.hpp"

namespace dvrk {
    
    /*
     *                  Public Methods - Initialisation
     */
    
    DaVinciRobot::DaVinciRobot(ros::NodeHandle nh)
        : _nh(nh), _stereo_camera(nh) {
        // Do Nothing
    }
    
    
    void DaVinciRobot::setup_cameras() {
        _stereo_camera.subscribe(left_camera_topic, right_camera_topic);
    }
    

    /*
     *                  Public Methods - Endoscope Interface
     */
    
    void DaVinciRobot::setOnImageCallback(dvrk::StereoCamera::image_cb_t cb) {
        _stereo_camera.set_on_image_callback(std::move(cb));
    }
    
    bool DaVinciRobot::wait_for_first_image() const {
        return _stereo_camera.wait_for_first_image();
    }
    
    
    bool DaVinciRobot::endoscope_has_images() const {
        return _stereo_camera.has_images();
    }
    
    
    uint32_t DaVinciRobot::get_endoscope_image_count() const {
        return _stereo_camera.get_image_count();
    }
    
    
    const dvrk::StereoImage DaVinciRobot::get_stereo_image() const {
        return _stereo_camera.get_latest_image();
    }
    
    const sensor_msgs::ImageConstPtr DaVinciRobot::get_left_image() const {
        return  _stereo_camera.get_left_image();
    }
    
    const sensor_msgs::ImageConstPtr DaVinciRobot::get_right_image() const {
        return  _stereo_camera.get_right_image();
    }
    
}
