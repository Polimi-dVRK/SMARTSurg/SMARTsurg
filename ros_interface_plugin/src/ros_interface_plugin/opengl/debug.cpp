//
// Created by tibo on 30/04/18.
//

#include <plog/Log.h>
#include "ros_interface_plugin/opengl/debug.hpp"

namespace dvrk {
    
    bool check_gl_ok() {
        GLenum error_code = glGetError();
        if (error_code == GL_NO_ERROR)
            return true;
        
        LOG_ERROR << "OpenGL Error: " << gl_enum_to_string(error_code);
        return false;
    }
    
    const std::string gl_enum_to_string(GLenum glvalue) {
        switch(glvalue) {
            case GL_TRUE: return "GL_TRUE";
            case GL_FALSE: return "GL_FALSE";
            
            case GL_VERTEX_SHADER: return "GL_VERTEX_SHADER";
            case GL_GEOMETRY_SHADER: return "GL_GEOMETRY_SHADER";
            case GL_FRAGMENT_SHADER: return "GL_FRAGMENT_SHADER";
            
            case GL_SHADER_TYPE: return "GL_SHADER_TYPE";
            case GL_DELETE_STATUS: return "GL_DELETE_STATUS";
            case GL_COMPILE_STATUS: return "GL_COMPILE_STATUS";
            case GL_INFO_LOG_LENGTH: return "GL_INFO_LOG_LENGTH";
            case GL_SHADER_SOURCE_LENGTH: return "GL_SHADER_SOURCE_LENGTH";
            
            case GL_LINK_STATUS: return "GL_LINK_STATUS";
            case GL_VALIDATE_STATUS: return "GL_VALIDATE_STATUS";
            case GL_ATTACHED_SHADERS: return "GL_ATTACHED_SHADERS";
            case GL_ACTIVE_ATTRIBUTES: return "GL_ACTIVE_ATTRIBUTES";
            case GL_ACTIVE_ATTRIBUTE_MAX_LENGTH: return "GL_ACTIVE_ATTRIBUTE_MAX_LENGTH";
            case GL_ACTIVE_UNIFORMS: return "GL_ACTIVE_UNIFORMS";
            case GL_ACTIVE_UNIFORM_BLOCKS: return "GL_ACTIVE_UNIFORM_BLOCKS";
            case GL_ACTIVE_UNIFORM_BLOCK_MAX_NAME_LENGTH: return "GL_ACTIVE_UNIFORM_BLOCK_MAX_NAME_LENGTH";
            case GL_ACTIVE_UNIFORM_MAX_LENGTH: return "GL_ACTIVE_UNIFORM_MAX_LENGTH";
            case GL_TRANSFORM_FEEDBACK_BUFFER_MODE: return "GL_TRANSFORM_FEEDBACK_BUFFER_MODE";
            case GL_TRANSFORM_FEEDBACK_VARYINGS: return "GL_TRANSFORM_FEEDBACK_VARYINGS";
            case GL_TRANSFORM_FEEDBACK_VARYING_MAX_LENGTH: return "GL_TRANSFORM_FEEDBACK_VARYING_MAX_LENGTH";
            case GL_GEOMETRY_VERTICES_OUT: return "GL_GEOMETRY_VERTICES_OUT";
            case GL_GEOMETRY_INPUT_TYPE: return "GL_GEOMETRY_INPUT_TYPE";
            case GL_GEOMETRY_OUTPUT_TYPE: return "GL_GEOMETRY_OUTPUT_TYPE";
            
            case GL_TRIANGLES: return "GL_TRIANGLES";
            
            case GL_POINT: return "GL_POINT";
            case GL_LINE: return "GL_LINE";
            case GL_FILL: return "GL_FILL";
            
            case GL_FRONT_AND_BACK: return "GL_FRONT_AND_BACK";
            
            case GL_ARRAY_BUFFER: return "GL_ARRAY_BUFFER";
            case GL_ELEMENT_ARRAY_BUFFER: return "GL_ELEMENT_ARRAY_BUFFER";
            case GL_PIXEL_PACK_BUFFER: return "GL_PIXEL_PACK_BUFFER";
            case GL_PIXEL_UNPACK_BUFFER: return "GL_PIXEL_UNPACK_BUFFER";
            case GL_COPY_READ_BUFFER: return "GL_COPY_READ_BUFFER";
            case GL_COPY_WRITE_BUFFER: return "GL_COPY_WRITE_BUFFER";
            case GL_TEXTURE_BUFFER: return "GL_TEXTURE_BUFFER";
            case GL_TRANSFORM_FEEDBACK_BUFFER: return "GL_TRANSFORM_FEEDBACK_BUFFER";
            case GL_UNIFORM_BUFFER: return "GL_UNIFORM_BUFFER";
            
            case GL_STREAM_DRAW: return "GL_STREAM_DRAW";
            case GL_STREAM_READ: return "GL_STREAM_READ";
            case GL_STREAM_COPY: return "GL_STREAM_COPY";
            case GL_STATIC_DRAW: return "GL_STATIC_DRAW";
            case GL_STATIC_READ: return "GL_STATIC_READ";
            case GL_STATIC_COPY: return "GL_STATIC_COPY";
            case GL_DYNAMIC_DRAW: return "GL_DYNAMIC_DRAW";
            case GL_DYNAMIC_READ: return "GL_DYNAMIC_READ";
            case GL_DYNAMIC_COPY: return "GL_DYNAMIC_COPY";
                
                // GL texture types
            case GL_TEXTURE_2D: return "GL_TEXTURE_2D";
            case GL_PROXY_TEXTURE_2D: return "GL_PROXY_TEXTURE_2D";
            case GL_TEXTURE_1D_ARRAY: return "GL_TEXTURE_1D_ARRAY";
            case GL_PROXY_TEXTURE_1D_ARRAY: return "GL_PROXY_TEXTURE_1D_ARRAY";
            case GL_TEXTURE_RECTANGLE: return "GL_TEXTURE_RECTANGLE";
            case GL_PROXY_TEXTURE_RECTANGLE: return "GL_PROXY_TEXTURE_RECTANGLE";
            case GL_TEXTURE_CUBE_MAP_POSITIVE_X: return "GL_TEXTURE_CUBE_MAP_POSITIVE_X";
            case GL_TEXTURE_CUBE_MAP_NEGATIVE_X: return "GL_TEXTURE_CUBE_MAP_NEGATIVE_X";
            case GL_TEXTURE_CUBE_MAP_POSITIVE_Y: return "GL_TEXTURE_CUBE_MAP_POSITIVE_Y";
            case GL_TEXTURE_CUBE_MAP_NEGATIVE_Y: return "GL_TEXTURE_CUBE_MAP_NEGATIVE_Y";
            case GL_TEXTURE_CUBE_MAP_POSITIVE_Z: return "GL_TEXTURE_CUBE_MAP_POSITIVE_Z";
            case GL_TEXTURE_CUBE_MAP_NEGATIVE_Z: return "GL_TEXTURE_CUBE_MAP_NEGATIVE_Z";
            case GL_PROXY_TEXTURE_CUBE_MAP: return "GL_PROXY_TEXTURE_CUBE_MAP";
                
                // GL internal formats
            case GL_RGBA32F: return "GL_RGBA32F";
            case GL_RGBA32I: return "GL_RGBA32I";
            case GL_RGBA32UI: return "GL_RGBA32UI";
            case GL_RGBA16: return "GL_RGBA16";
            case GL_RGBA16F: return "GL_RGBA16F";
            case GL_RGBA16I: return "GL_RGBA16I";
            case GL_RGBA16UI: return "GL_RGBA16UI";
            case GL_RGBA8: return "GL_RGBA8";
            case GL_RGBA8UI: return "GL_RGBA8UI";
            case GL_SRGB8_ALPHA8: return "GL_SRGB8_ALPHA8";
            case GL_RGB10_A2: return "GL_RGB10_A2";
            case GL_RGB10_A2UI: return "GL_RGB10_A2UI";
            case GL_R11F_G11F_B10F: return "GL_R11F_G11F_B10F";
            case GL_RG32F: return "GL_RG32F";
            case GL_RG32I: return "GL_RG32I";
            case GL_RG32UI: return "GL_RG32UI";
            case GL_RG16: return "GL_RG16";
            case GL_RG16F: return "GL_RG16F";
            case GL_RG8: return "GL_RG8";
            case GL_RG8I: return "GL_RG8I";
            case GL_RG8UI: return "GL_RG8UI";
            case GL_R32F: return "GL_R32F";
            case GL_R32I: return "GL_R32I";
            case GL_R32UI: return "GL_R32UI";
            case GL_R16F: return "GL_R16F";
            case GL_R16I: return "GL_R16I";
            case GL_R16UI: return "GL_R16UI";
            case GL_R8: return "GL_R8";
            case GL_R8I: return "GL_R8I";
            case GL_R8UI: return "GL_R8UI";
            case GL_RGBA16_SNORM: return "GL_RGBA16_SNORM";
            case GL_RGBA8_SNORM: return "GL_RGBA8_SNORM";
            case GL_RGB32F: return "GL_RGB32F";
            case GL_RGB32I: return "GL_RGB32I";
            case GL_RGB32UI: return "GL_RGB32UI";
            case GL_RGB16_SNORM: return "GL_RGB16_SNORM";
            case GL_RGB16F: return "GL_RGB16F";
            case GL_RGB16I: return "GL_RGB16I";
            case GL_RGB16UI: return "GL_RGB16UI";
            case GL_RGB16: return "GL_RGB16";
            case GL_RGB8_SNORM: return "GL_RGB8_SNORM";
            case GL_RGB8: return "GL_RGB8";
            case GL_RGB8I: return "GL_RGB8I";
            case GL_RGB8UI: return "GL_RGB8UI";
            case GL_SRGB8: return "GL_SRGB8";
            case GL_RGB9_E5: return "GL_RGB9_E5";
            case GL_RG16_SNORM: return "GL_RG16_SNORM";
            case GL_RG8_SNORM: return "GL_RG8_SNORM";
            case GL_COMPRESSED_RG_RGTC2: return "GL_COMPRESSED_RG_RGTC2";
            case GL_COMPRESSED_SIGNED_RG_RGTC2: return "GL_COMPRESSED_SIGNED_RG_RGTC2";
            case GL_R16_SNORM: return "GL_R16_SNORM";
            case GL_R8_SNORM: return "GL_R8_SNORM";
            case GL_COMPRESSED_RED_RGTC1: return "GL_COMPRESSED_RED_RGTC1";
            case GL_COMPRESSED_SIGNED_RED_RGTC1: return "GL_COMPRESSED_SIGNED_RED_RGTC1";
            case GL_DEPTH_COMPONENT32F: return "GL_DEPTH_COMPONENT32F";
            case GL_DEPTH_COMPONENT24: return "GL_DEPTH_COMPONENT24";
            case GL_DEPTH_COMPONENT16: return "GL_DEPTH_COMPONENT16";
            case GL_DEPTH32F_STENCIL8: return "GL_DEPTH32F_STENCIL8";
            case GL_DEPTH24_STENCIL8: return "GL_DEPTH24_STENCIL8";
                
                // Texture settings
            case GL_TEXTURE_BASE_LEVEL: return "GL_TEXTURE_BASE_LEVEL";
            case GL_TEXTURE_COMPARE_FUNC: return "GL_TEXTURE_COMPARE_FUNC";
            case GL_TEXTURE_COMPARE_MODE: return "GL_TEXTURE_COMPARE_MODE";
            case GL_TEXTURE_LOD_BIAS: return "GL_TEXTURE_LOD_BIAS";
            case GL_TEXTURE_MIN_FILTER: return "GL_TEXTURE_MIN_FILTER";
            case GL_TEXTURE_MAG_FILTER: return "GL_TEXTURE_MAG_FILTER";
            case GL_TEXTURE_MIN_LOD: return "GL_TEXTURE_MIN_LOD";
            case GL_TEXTURE_MAX_LOD: return "GL_TEXTURE_MAX_LOD";
            case GL_TEXTURE_MAX_LEVEL: return "GL_TEXTURE_MAX_LEVEL";
            case GL_TEXTURE_SWIZZLE_R: return "GL_TEXTURE_SWIZZLE_R";
            case GL_TEXTURE_SWIZZLE_G: return "GL_TEXTURE_SWIZZLE_G";
            case GL_TEXTURE_SWIZZLE_B: return "GL_TEXTURE_SWIZZLE_B";
            case GL_TEXTURE_SWIZZLE_A: return "GL_TEXTURE_SWIZZLE_A";
            case GL_TEXTURE_WRAP_S: return "GL_TEXTURE_WRAP_S";
            case GL_TEXTURE_WRAP_T: return "GL_TEXTURE_WRAP_T";
            case GL_TEXTURE_WRAP_R: return "GL_TEXTURE_WRAP_R";
                
                // GL pixel formats
            case GL_RED: return "GL_RED";
            case GL_RG: return "GL_RG";
            case GL_RGB: return "GL_RGB";
            case GL_BGR: return "GL_BGR";
            case GL_RGBA: return "GL_RGBA";
            case GL_BGRA: return "GL_BGRA";
                
                // GL pixel data types
            case GL_UNSIGNED_BYTE: return "GL_UNSIGNED_BYTE";
            case GL_BYTE: return "GL_BYTE";
            case GL_UNSIGNED_SHORT: return "GL_UNSIGNED_SHORT";
            case GL_SHORT: return "GL_SHORT";
            case GL_UNSIGNED_INT: return "GL_UNSIGNED_INT";
            case GL_INT: return "GL_INT";
            case GL_FLOAT: return "GL_FLOAT";
            case GL_UNSIGNED_BYTE_3_3_2: return "GL_UNSIGNED_BYTE_3_3_2";
            case GL_UNSIGNED_BYTE_2_3_3_REV: return "GL_UNSIGNED_BYTE_2_3_3_REV";
            case GL_UNSIGNED_SHORT_5_6_5: return "GL_UNSIGNED_SHORT_5_6_5";
            case GL_UNSIGNED_SHORT_5_6_5_REV: return "GL_UNSIGNED_SHORT_5_6_5_REV";
            case GL_UNSIGNED_SHORT_4_4_4_4: return "GL_UNSIGNED_SHORT_4_4_4_4";
            case GL_UNSIGNED_SHORT_4_4_4_4_REV: return "GL_UNSIGNED_SHORT_4_4_4_4_REV";
            case GL_UNSIGNED_SHORT_5_5_5_1: return "GL_UNSIGNED_SHORT_5_5_5_1";
            case GL_UNSIGNED_SHORT_1_5_5_5_REV: return "GL_UNSIGNED_SHORT_1_5_5_5_REV";
            case GL_UNSIGNED_INT_8_8_8_8: return "GL_UNSIGNED_INT_8_8_8_8";
            case GL_UNSIGNED_INT_8_8_8_8_REV: return "GL_UNSIGNED_INT_8_8_8_8_REV";
            case GL_UNSIGNED_INT_10_10_10_2: return "GL_UNSIGNED_INT_10_10_10_2";
            case GL_UNSIGNED_INT_2_10_10_10_REV: return "GL_UNSIGNED_INT_2_10_10_10_REV";
            
            default: return "UNKNOWN";
        }
    }
}
