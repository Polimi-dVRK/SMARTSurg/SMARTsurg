#
# This gdbinit file makes it easier to debug Unity and any loaded native plugins.
# You may need to add `set auto-load safe-path /` to your `~/.gdbinit` file to tell gdb that it
# is allowed to load this one. 
#

# Don't stop execution for these signals
handle SIGPWR noprint
handle SIGXCPU noprint

# Load debugging symbols for the native plugin from here
set solib-search-path SMARTSurg_GUI/Assets/Plugins

